//import Swiper from './assets/swiper/package/js/swiper.esm.browser.bundle.min.js';

import Swiper from './node_modules/swiper/swiper-bundle.js';


//document.addEventListener('DOMContentLoaded', function() {

window.addEventListener('load', function() {
    //console.log('test');

    new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: true,
        //effect: 'fade',
        autoplay: {
            delay: 5000
        },
        //fadeEffect: {
        //	crossFade: true
        //},
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true
            //type: 'progressbar',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    })

});



// Parallax Effekt
/*
window.addEventListener('scroll', function(e){
	const target = document.querySelectorAll('.scroll');
	
	var scrolled = window.pageYOffset;
	var rate = scrolled * -20.5;
	
	target.style.transform = 'translate3d(0px,'+rate+'px, 0px)';
	
	var index = 0, length = target.length;
	for(index;index < length; index++) {
		var pos = window.pageYOffset * target[index].dataset.rate;
		
		if(target[index].dataset.direction === 'vertical') {
			target[index].style.transform = 'translate3d(0px,'+pos+'px, 0px)';
		} else {
			var posX = window.pageYOffset * target[index].dataset.rateX;
			var posY = window.pageYOffset * target[index].dataset.rateY;
			target[index].style.transform = 'translate3d('+posX+'px,'+posY+'px, 0px)';
		}
		
		
	}
});
*/

function scrollAppear(){
    var introText = document.querySelector('.fadeIn');
	
    var introPosition = introText.getBoundingClientRect().top;
    console.log(introPosition);
	
    var screenPosition = window.innerHeight / 1.3;
	
    if(introPosition < screenPosition) {
        introText.classList.add('show');
    }
}

window.addEventListener('scroll', scrollAppear);

scrollAppear();
